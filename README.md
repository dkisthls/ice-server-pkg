# ice-server-pkg #

This repository contains the bits necessary to build an rpm and deb that
includes the atst-ice-event and atst-ice-connection systemd units and associated
configuration files.

### Dependencies ###

* Ubuntu
* fpm:
    * https://github.com/jordansissel/fpm
    * https://fpm.readthedocs.io/en/latest/installing.html
* rpm: apt install rpm

### building package ###

run 
```shell
./mkpkgs 
``` 
from the top of the tree 

### Notes ###

The mkpkgs script has the CURRENT version hard coded in it.  If you are
developing a new package you will need to update the version in addition to any
other changes you are making.  Be sure to commit the new version number (plus
the changes) when you are done
